#!/usr/bin/env python3
from app import create_app, config

app = create_app(config['flask'])

if __name__ == '__main__':
    app.run(**config['runtime'])
