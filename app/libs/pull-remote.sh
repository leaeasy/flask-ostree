#!/bin/bash
# pull http://10.0.10.32/gnome/repo/ org.gnome.eog/x86_64/stable

REPO=${REPO:-/srv/flatpak/repo}

remote_url=$1
remote_ref=$2

if [ -z ${remote_url} ] || [ -z ${remote_ref} ];then
	echo "Usage: $0 remote_url remote_ref"
	exit 1
fi


ostree --repo=${REPO} remote delete flatdeb-worker 2>/dev/null || true

ostree --repo=${REPO} remote add --no-gpg-verify flatdeb-worker ${remote_url}

case ${remote_ref} in 
    runtime/*)
        ostree --repo=${REPO} pull --disable-fsync --mirror --untrusted flatdeb-worker ${remote_ref}
        exit_code=$?
        ;;
    *)
        ostree --repo=${REPO} pull --disable-fsync --mirror --untrusted flatdeb-worker app/${remote_ref}
        exit_code=$?
        locale=$(echo ${remote_ref} | awk -F'/' '{print $1 ".Locale" "/" $2 "/" $3}' | sed 's/-/_/g')
        ostree --repo=${REPO} pull --disable-fsync --mirror --untrusted flatdeb-worker runtime/${locale} || true
        ;;
esac

ostree --repo=${REPO} remote delete flatdeb-worker 
ostree --repo=${REPO} summary -u

exit ${exit_code}
