import os
import git
from flask import flash
from app import config

import threading
import sqlite3
import subprocess
import time
import datetime
import logging

def create_logger(level=logging.DEBUG):
    logger = logging.getLogger("log")
    logger.setLevel(level)
    fmter = logging.Formatter('[%(levelname)s] (%(asctime)s) %(message)s', datefmt='%a, %d %b %Y %H:%M:%S')
    hdlr = logging.FileHandler(config['flask'].get('log'))
    hdlr.setFormatter(fmt=fmter)
    logger.addHandler(hdlr=hdlr)
    return logger

logger = create_logger()

import gi
gi.require_version('OSTree', '1.0')
from gi.repository import Gio, OSTree

class SqliteCache(threading.Thread):
    def __init__(self):
        super().__init__()
        self.conn=sqlite3.connect('cache.db', timeout=5, check_same_thread=False)
        self.init()

    def init(self):
        self.conn.execute('CREATE TABLE IF NOT EXISTS flatdeb (name, ref, size);')
        self.conn.execute('CREATE TABLE IF NOT EXISTS pulls (id integer PRIMARY KEY, username TEXT, origin TEXT, ref TEXT, status TEXT, begin_date TIMESTAMP, status_change TIMESTAMP);')

        self.conn.commit()

    def update_pull(self, id, status):
        self.conn.execute('UPDATE pulls SET status=?,status_change=? WHERE id=?;' , (status, datetime.datetime.now(), id))
        self.conn.commit()

    def add_pull(self, username, origin, ref):
        self.conn.execute('INSERT INTO pulls (username, origin, ref, status, begin_date) VALUES (?,?,?,?,?);' , (username, origin, ref, 'wait', datetime.datetime.now()))
        self.conn.commit()

    def get_pull(self, status='wait'):
        c = self.conn.execute('SELECT * FROM pulls WHERE status="%s"' % status)
        result = c.fetchone()
        return result

    def execute(self, args):
        c = self.conn.execute(args)
        results = c.fetchall()
        return results

    def update_flatdeb(self, name, ref, size):
        self.conn.execute('UPDATE flatdeb SET size="%s" WHERE name="%s" AND ref="%s"' % (size, name, ref))
        self.conn.commit()

    def cache_flatdeb(self, name, ref, size):
        self.conn.execute("INSERT INTO flatdeb VALUES (?, ?, ?);", (name, ref, size))
        self.conn.commit()

    def get_calcutor_ref(self):
        c = self.conn.execute('SELECT * FROM flatdeb WHERE size="-1"')
        result = c.fetchone()
        return result

    def get_from_flatdeb(self, name, ref):
        c = self.conn.execute('SELECT * FROM flatdeb WHERE name="%s" AND ref="%s"' % (name, ref))
        results = c.fetchall()
        return results

cache = SqliteCache()

class Flatdeb():
    def __init__(self):
        self.pkgname = None
        self.description = None
        self.yaml = None

def _git_update():
    counter = 0
    while True:
        if counter == 10:
            package_dir = config['flask'].get('package_dir')
            try:
                subprocess.check_call("git fetch -p --all", shell=True, cwd=package_dir, stdout=open('/dev/null', 'w'))
                subprocess.check_call("git merge", shell=True, cwd=package_dir, stdout=open('/dev/null', 'w'))
            except subprocess.CalledProcessError:
                logger.warning("git update")

            counter = 0
        time.sleep(5)
        counter += 1


def get_flatdebs():
    ostree_dir = config['flask'].get('ostree_dir')
    flatdeb_dir = os.path.join(ostree_dir, "refs/heads/app")
    flatdebs = {}
    for pkgname in os.listdir(flatdeb_dir):
        directory = os.path.join(flatdeb_dir, pkgname)
        flatdeb = Flatdeb()
        flatdeb.pkgname = pkgname
        flatdeb.description = pkgname
        try:
            flatdebs[pkgname] = flatdeb
        except: 
            flash('{} is an invalid git directory.'.format(pkgname))
    return flatdebs

def get_commitsize(ref, calculate=False):
    ostree_dir = config['flask'].get('ostree_dir')
    r = OSTree.Repo.new(Gio.File.new_for_path(ostree_dir))
    r.open(None)
    try:
        [_, rev ] = r.resolve_rev(ref, False)
    except Exception as e:
        return None

    if len(cache.get_from_flatdeb(ref, rev)) != 0:
        return cache.get_from_flatdeb(ref, rev)[0][-1]

    if calculate:
        cache.cache_flatdeb(ref, rev, '-1')
    return '-1'

finish_event = threading.Event()
def _calcutor_commitsize():
    ostree_dir = config['flask'].get('ostree_dir')
    r = OSTree.Repo.new(Gio.File.new_for_path(ostree_dir))
    r.open(None)
    counter = 0
    while True:
        if finish_event.isSet() or counter == 10:
            cal_ref = cache.get_calcutor_ref()
            if cal_ref:
                ref, rev, size = cal_ref
                [_, reachable ] = r.traverse_commit(rev, 0 , None)
                total = 0
                for k, v in reachable.items():
                    csum, objtype = k.unpack()
                    if objtype == OSTree.ObjectType.FILE:
                        [_,_,fileinfo,_] = r.load_file(csum, None)
                        sz = fileinfo.get_size()
                    else:
                        [_,sz] = r.query_object_storage_size(objtype, csum, None)

                    total += sz

                cache.update_flatdeb(ref, rev, total)
                finish_event.clear()

            counter = 0
        else:
            time.sleep(1)
            counter += 1

def _ostree_update():
    ostree_dir = config['flask'].get('ostree_dir')
    update_script= config['flask'].get('ostree_update_script')
    counter = 0
    while True:
        if counter == 10:
            pull = cache.get_pull()
            if pull:
                p_id = pull[0]
                p_origin = pull[2]
                p_ref = pull[3]
                try:
                    update_cmd = "%s %s %s" % (update_script, p_origin, p_ref)
                    subprocess.check_call(update_cmd, shell=True, stdout=open('/dev/null', 'w'))
                except subprocess.CalledProcessError as e:
                    logger.warning("Update ostree fail - %s %s" % (p_origin, p_ref))
                    cache.update_pull(p_id, status='fail')
                else:
                    logger.info("Update ostree done - %s %s" % (p_origin, p_ref))
                    cache.update_pull(p_id, status='ok')
            counter = 0
        else:
            time.sleep(60)
            counter += 1
