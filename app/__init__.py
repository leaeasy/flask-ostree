import toml
from flask import Flask

import _thread

def load_config():
    with open('config.toml') as config_file:
        config = toml.loads(config_file.read())
    return config

config = load_config()
def create_app(configs):
    app = Flask(__name__)
    app.config.update(**configs)

    # Blueprints
    from app.blueprints.main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from app.blueprints.changes import changes as changes_blueprint
    app.register_blueprint(changes_blueprint, url_prefix='/changes')

    from app.blueprints.resources import resources as resources_blueprint
    app.register_blueprint(resources_blueprint, url_prefix='/resources')

    from app.blueprints.pulls import pulls as pulls_blueprint
    app.register_blueprint(pulls_blueprint, url_prefix='/pulls')

    from app.blueprints.auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from app.libs.projects import _calcutor_commitsize, _git_update, _ostree_update
    _thread.start_new_thread(_calcutor_commitsize, ())
    _thread.start_new_thread(_git_update, ())
    _thread.start_new_thread(_ostree_update, ())

    return app
