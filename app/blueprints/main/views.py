from flask import render_template
from flask import Response
from . import main
from app.libs.projects import get_flatdebs
from app import config
import os
from flask import abort, redirect
import subprocess

@main.route('/', methods=['GET'])
def index():
    flatdebs = get_flatdebs()
    return render_template('main/index.html', flatdebs=flatdebs)

@main.route('/install/<pkgname>', methods=['GET'])
def install(pkgname):
    result = '<pre style="overflow: hidden !important;">'
    result += "flatpak --user remote-add --no-gpg-verify flatdeb %s" % config['flask'].get('repo_url')
    result += "</br>"
    result += "flatpak --user install flatdeb %s" % pkgname
    result += "</pre>"
    return result

@main.route('/appstream/<pkgname>.flatpakref', methods=['GET'])
def appstream(pkgname):
    result = '''[Flatpak Ref]
Name=%s
Branch=master
Title=%s from flatdeb
Url=http://10.0.10.32/repo
IsRuntime=false
''' % (pkgname, pkgname)
    return Response(result, mimetype="application/vnd.flatpak.ref")

@main.route('/<pkgname>', methods=['GET'])
def get(pkgname):
    flatdebs = get_flatdebs()
    flatdeb = flatdebs.get(pkgname)
    if not flatdeb:
        return abort(404)

    flatdeb.repo_url = config['flask'].get('repo_url')
    ostreedir = config['flask'].get('ostree_dir')
    app_ostree = os.path.join(ostreedir, 'refs/heads/app', pkgname, 'x86_64')
    for branch in os.listdir(app_ostree):
        if os.path.exists(os.path.join(app_ostree, branch)):
            _, output = subprocess.getstatusoutput(" ".join(['ostree', '--repo=%s' % ostreedir, 'show', 'app/%s/x86_64/%s' % (pkgname, branch)]))
            flatdeb.ostree = output
            break

    project_dir = config['flask'].get('package_dir')
    pkgname = pkgname.split('.')[-1]
    if os.path.exists(os.path.join(project_dir, pkgname + ".yaml")):
        with open(os.path.join(project_dir, pkgname + ".yaml")) as fp:
            flatdeb.yaml = fp.read()
    else:
        flatdeb.yaml = os.path.join(project_dir, pkgname + ".yaml")
    return render_template('flatdebs/get.html', flatdeb=flatdeb)
