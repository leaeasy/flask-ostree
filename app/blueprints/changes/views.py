from flask import abort, render_template
from . import changes
import git
from app import config

@changes.route('/', methods=['GET'])
def index():
    package = config['flask'].get('package_dir')
    try:
        project = git.Repo(package)
    except git.exc.InvalidGitRepositoryError:
        return abort(404)
    return render_template('changes/index.html', project=project)
