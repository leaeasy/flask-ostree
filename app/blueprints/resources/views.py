from . import resources
from app import config
import os
from flask import request, abort
from flask import send_file, jsonify
from app.libs.projects import get_commitsize

@resources.route('/icons/<pkgname>', methods=['GET'])
def get_icon(pkgname, fallback='application-x-executable.svg'):
    pkgname = pkgname.split('.')[-1]
    icons = config['flask'].get('icons_dir')
    has_icon = False
    for ext in ['svg', 'svgz', 'png']:
        icon_file = os.path.join(icons, pkgname + '.' + ext)
        if os.path.exists(icon_file):
            has_icon = True
            break

    if has_icon is False:
        icon_file = os.path.join(icons, fallback)
        if not os.path.exists(icon_file):
            abort(404)

    return send_file(icon_file)

@resources.route('/cache', methods=['GET'])
def get_cache_commit(fallback=0):
    ref = request.args.get('ref', None)
    commitsize = 'Unknown'
    if ref.startswith('/'):
        ref = ref[1:]
    if ref is not None:
        commitsize = get_commitsize('app/' + ref, calculate=False)
    return jsonify({'commitsize': commitsize})

@resources.route('/commit', methods=['GET'])
def commit_calculate(fallback=0):
    ref = request.args.get('ref', None)
    if ref.startswith('/'):
        ref = ref[1:]
    if ref is not None:
        commitsize = get_commitsize('app/' + ref, calculate=True)
    return jsonify({'commitsize': commitsize})
