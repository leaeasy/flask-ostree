from flask import abort, render_template, request
from . import pulls
import git
from app import config
from app.libs.projects import cache, logger

@pulls.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        username = request.form.get('username', None)
        origin = request.form.get('origin', None)
        ref = request.form.get('ref', None)
        if username is None or origin is None or ref is None:
            logger.error('add ostree pull - %s' % str(request.form))
            abort(503)

        cache.add_pull(username, origin, ref)
        logger.info('add ostree pull - %s %s %s' % (username, origin, ref))
        return 'add successful'

    elif request.method == 'GET':
        pulls = cache.execute('SELECT * FROM pulls ORDER BY -id')
        return render_template('pulls/index.html', pulls=pulls)
